TY  - JOUR
DB  - PMC
AU  - Mallory, Allison
AU  - Shkumatava, Alena
T1  - LncRNAs in Vertebrates: Advances and Challenges
SN  - 0300-9084
SN  - 1638-6183
Y1  - 2015/10/
PY  - 2015/03/24
AB  - Beyond the handful of classic and well-characterized long noncoding RNAs (lncRNAs), more recently, hundreds of thousands of lncRNAs have been identified in multiple species including bacteria, plants and vertebrates, and the number of newly annotated lncRNAs continues to increase as more transcriptomes are analyzed. In vertebrates, the expression of many lncRNAs is highly regulated, displaying discrete temporal and spatial expression patterns, suggesting roles in a wide range of developmental processes and setting them apart from classic housekeeping ncRNAs. In addition, the deregulation of a subset of these lncRNAs has been linked to the development of several diseases, including cancers, as well as developmental anomalies. However, the majority of vertebrate lncRNA functions remain enigmatic. As such, a major task at hand is to decipher the biological roles of lncRNAs and uncover the regulatory networks upon which they impinge. This review focuses on our emerging understanding of lncRNAs in vertebrate animals, highlighting some recent advances in their functional analyses across several species and emphasizing the current challenges researchers face to characterize lncRNAs and identify their in vivo functions. 
SP  - 3
EP  - 14
VL  - 117
DO  - 10.1016/j.biochi.2015.03.014
AN  - PMC5125525
UR  - http://www.ncbi.nlm.nih.gov/pmc/articles/PMC5125525/
U1  - 25812751[pmid]
J1  - Biochimie
JF  - Biochimie
ER  - 
