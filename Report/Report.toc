\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Methods}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Binomial}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Multinomial}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Multilabel}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Results}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Methods evaluation}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Tables and Statistics}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Performance improvement and additional data}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Conclusion}{13}
