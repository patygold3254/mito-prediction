\documentclass{article}
\usepackage{graphicx}
\usepackage[backend=biber, bibencoding=utf8,style=numeric,sorting=none]{biblatex}
\usepackage{graphicx}
\graphicspath{ {./images/} }
\bibliography{my_bib}

\begin{document}

\title{Prediction of non-coding RNA mitochondrial function by machine learning}
\author{Patricia Figueira Goldberg}

\maketitle

\begin{abstract}

Mitochondrial diseases involve genetic mutations in mitochondrial DNA (mtDNA) and nuclear DNA (nDNA), and it represents the largest class of inherited metabolic diseases
affecting around 20 in 100,000 births.

The goal of this guided research was to use the multi-tissue GTEx RNA Sequence data, which is a reference resource for gene expression level, combined with machine learning in order to predict non-coding RNAs with a probable mitochondrial function.

After understanding the data and evaluating the classification model, this project applied the model and used the prediction results of the non-coding genes to prioritize and further validate it if it encodes protein or not.

\end{abstract}

\newpage

\tableofcontents

\newpage

\section*{Acknowledgments}


I would like to express my deep gratitude to Professor Julien Gagneur, my research supervisor, for his guidance and useful critiques of this research work.

I would also like to thank Vicente Yépez, my research advisor, for his advice, assistance, guidance and teaching.

I would also like to extend my thanks to all people of the Gagneur lab for helping me through this semester.

\newpage

\section{Introduction}
Mitochondrial diseases represent the largest class of inherited metabolic diseases affecting around 20 in 100,000 births \cite{mtexplain}. They involve genetic mutations in mitochondrial DNA (mtDNA) and nuclear DNA (nDNA) \cite{mtexplain}. Efforts have been made to detect these genetic mutations associated with mitochondrial diseases in protein-coding RNA (a.k.a.\ coding RNA) \cite{NCRNA}, having an up to date list of 315 known disease causal genes \cite{mitochondrial_disease}, but a large class of RNAs are not protein-coding (a.k.a.\ non-coding RNA) from which the functions are by enlarge not understood \cite{Mallory2015}.

The GTEx (Genotype-Tissue Expression) project is a reference resource of gene expression data which aims to characterize the variation in the gene expression across individuals and tissues of the human body \cite{gtex}. The aim of this Guided Research project was to use the GTEx RNA Seq data combined with machine learning in order to predict non-coding RNAs with a probable mitochondrial function. The GTEx matrix used as an input had gene expression from both coding RNA (19,828 genes) and non-coding RNA (35,191 genes) from 9,592 samples taken from 54 different tissues. Together with the GTEx matrix, we can use other sources of information, such as the oxygen consumption rates (OCR) \cite{OCR}, the Compartments score, which uses a sequence-based prediction method to generate a confidence score to determine sub-cellular localization of the gene \cite{compartments}, or the CRISPR, which are scores obtained from CRISPR assays related to oxidative phosphorylation.

To understand the mitochondrial role in disease, it is useful to have an inventory of its protein components \cite{mc}. For that, we can use an inventory called MitoCarta2.0 (we will use the notation MitoCarta in this report) which combined several data sources in order to provide a list of 1,158 human genes predicted as mitochondrial proteins \cite{mc}.

Due to the increasing number of research in Genomics, the Gene Ontology (GO) Consortium was developed in order to produce a structured vocabulary for describing the roles of genes and gene products in any organism, divided into three categories: Biological Process, Molecular Function, and Cellular Component \cite{GO}. The Cellular Component is the category that refers to the cell localization where a gene product is active \cite{GO}. In this project, we used GO slim terms, which are cut-down versions of the GO ontologies containing a subset of the terms in GO, e.g.\ the GO Slim term for Mitochondrion is GO:0005739 while for Nucleus is GO:0005634 \cite{cellularcomponent}.

Complementary to the GO terms, the HUGO Gene Nomenclature Committee (HGNC) assigns unique symbols and names to human genes \cite{genenames}. HGNC contains data from 1,260 different Gene Families which contains groups of related genes \cite{respiratory}. In our case we are more interested in the Gene Family: Mitochondrial respiratory chain complexes (we will use the RCC notation), which is subdivided into five mitochondrial complexes \cite{respiratory}.

\section{Methods}
Machine Learning can be defined as a set of methods that can detect patterns in the input data \( x \) to predict an unknown output \( y \) \cite{murphy2012machine}. In this research, the GTEx matrix was the main \( X \) used in several different supervised learning methods. To minimize the effects of the curse of dimensionality, a well-known concept in Machine Learning which associates a high dimensional matrix with poor prediction performance \cite{murphy2012machine}, we reduced the number of samples randomly from 9,592 to 1,000 in our train and test data. The counts are numerical features which were normalized using a \( log_2\) -transformation \( log_2( \frac{k}{s}+1) \), where \( k \) is the count and \( s \) is the size factor, and was centered by subtracting the row means. In addition to the GTEx data, other features were included in some models.

Although the output \( y \) is a different matter, since it changes accordingly with the purpose of each classifier, it is mainly a binary classification (i.e.: TRUE or FALSE). The exception will be the classification model in which \( y \) is a vector of strings.

One important first step of data analysis, regardless of the method used, is to understand the data used. This helps us decide which methods to apply in this specific data set and it is essential in order to decide if any other data transformation method, besides the ones cited above, will be used.

\begin{figure}[h]
\includegraphics[width=8cm]{Tissues.png}
\caption{Barplot representing the number of available samples per tissue}
\centering
\label{barplot}
\end{figure}

The barplot (figure \ref{barplot}) was used to have an idea of the distribution of samples across different 54 tissues. The red line represents the count of 20 Samples, used as a reference only.

\begin{figure}[h]
\includegraphics[width=8cm]{HeatMap_Genes_Samples.png}
\caption{Heatmap of correlation between 2000 Genes and 1000 Samples}
\centering
\label{heatmap}
\end{figure}

The Heatmap (figure \ref{heatmap}), includes the correlation between Genes (rows), which contains both protein-coding and non-protein-coding genes, and Samples (columns). There are additional three columns in the heat map, which represent the genes that are classified as MitoCarta, Mito disease related or RCC genes, and two additional rows, which represents samples taken from different tissues. This heatmap can also be used as an unsupervised learning method due to the fact that it clusters the columns and rows using a dendrogram. We can see that the tissues cluster better together than the different classification of genes.

The machine learning algorithm which was most suitable in this case was GLMNET that fits the generalized linear models with elastic-net penalties \cite{glmnet}. We were able to use this method in two types of problems: Binomial and Multilabel. For Multinomial, we used Multi-Class logistic regression. As for the programming language, we used R as it is an suitable for statistical computations and visualization \cite{R_env}.

\subsection{Binomial}
When the \( y \) is a binary vector, the regularized logistic regression model is used. For parameters \( \big( \beta_0, \beta \big) \), the algorithm solves the penalized weighted least-squares of the problem
\[ max_{(\beta_0,\beta)} = \big\{ -l_Q(\beta_0,\beta) + \lambda P_{\alpha} (\beta) \big\} \]
where \( l_Q(\beta_0,\beta) \) is the unpenalized log-likelihood, \( P_{\alpha} (\beta) \) is a compromise between the ridge-regression penalty and the lasso penalty, and \( \lambda \) controls the penalty term \cite{glmnet}.

We trained four different models using this method to classify the following lists of genes:
\begin{itemize}
  \item Mito Carta (1,158 genes)
  \item Mito disease related (315 genes)
  \item Respiratory Chain Complex (137 genes)
  \item Mitochondrion GO slim (1,492 genes)
\end{itemize}

The \( y \) was created using the following logic:
\begin{verbatim}
if gene is in list and gene is protein_coding
    then y = TRUE
else if gene is not in list and gene is protein_coding
    then y = FALSE
else
     y = NA
\end{verbatim}

The train data used was the protein-coding genes (around 20,000) and the test data was all of the genes available in the dataset (more than 55,000).

\subsection{Multinomial}
Multinomial is no longer a binary classification, but has more than two classes as output having a rule that the output classes do not intersect \cite{multinom}. In our case, the \( y \) is a vector where each class that we wish to classify is represented using a string. This model has a form
\[ p(y = c|x, W) = \frac{exp(w_c^T x)}{\sum_{c'=1}^C exp(w_{c'}^T x)} \] where \( p(y = c|x, W) \) is the probability of \( y \) being of the class \( c \) given the input value \( x \) and the weighted matrix \( W \) \cite{murphy2012machine}.

One model was trained using a multinomial method, which had its \( y \) created using the following logic:
\begin{verbatim}
if gene is Mito Carta and gene is protein_coding
    then y = "MitoCarta"
else if gene is Ribosomal and gene is protein_coding
    then y = "Ribosomal"
else if gene is Plasma Membrane and gene is protein_coding
    then y = "Plasma_Membrane"
else if gene is protein_coding
    then y = FALSE
else
     y = NA
\end{verbatim}

This logic has a clear flaw due to the fact that genes can belong to more than one class and the order of the if function will change the final \( y \) vector. The Venn Diagram (figure \ref{venn}) shows that this is, in fact, true and it breaks that assumption above mentioned.

\begin{figure}[h]
\includegraphics[width=8cm]{Venn.png}
\caption{Venn Diagram of classes' intersection of Mito Carta, Ribosomal and Plasma Membrane}
\centering
\label{venn}
\end{figure}

\subsection{Multilabel}
Similar to Multinomial, Multilabel classification has also more than two classes (which can also be called as labels) as output \cite{multilabel_overview}, having a different implementation though. While Multinomial has only one vector as output, Multilabel has a matrix which each column of the matrix represents one desired class and the values of each column can be represented as binary again, TRUE if the row belongs to that specific class and FALSE otherwise. This allows the gene to belong to more than one category and we can use again GLMNET as the machine learning algorithm.

Although there are two different approaches to this classification problem, in this research we used only one, the so-called "Problem transformation". This approach tries to modify the problem into a simple binomial method \cite{multilabel_mlr}. In order to do that, there are five methods available: binary relevance, classifier chains, nested stacking, dependent binary relevance and stacking.

Binary relevance (BR) is the simplest method of all five which each label is classified independently of the others.

Classifier Chains (CC) trains the data of the label \( C_k \), with the target \( y_k \), using the matrix \( X \) and the targets \( y_1 \) until \( y_{k-1} \), taking in consideration the \( k \) position in the output matrix.

Nested stacking (NST) is similar to CC, but, instead of using the targets from the previous labels, it uses the predicted \( \hat{y}_1 \) until \( \hat{y}_{k-1} \).

Dependent binary relevance (DBR) trains the data of the \( C_k \), with the target \( y_k \), using all the other targets except \( y_k \), regardless \( k \) position in the output matrix.

Stacking (STA) is similar to DBR, but, instead of using the target \( y \), it uses the predicted \( \hat{y}_1 \) until \( \hat{y}_{m} \) where \( m \) is the number of labels.

This model was trained using as \( y's \) columns the twelve GO Slim terms for Cellular Components, including Mitochondrion, ER/Golgi, Cytoskeleton, Cytosol, Extracellular Matrix, Non Structural Extracellular, Nucleus, Other Cellular Components, Other Membranes, Other Organelles, Plasma Membrane and Translational Apparatus. Each column was created such as in the Binomial method.

\section{Results}

To better evaluate our models, we used the Precision-Recall curve (PR curve) instead of ROC curve due to the class imbalance problem our data has since our most "balanced" scenario is when \( y \) is Mito Carta and it represents only 5\% of our training data. Having TP, FP, TN, and FN as numbers of true positives, false positives, true negatives, and false negatives, respectively, the ROC curve's \( y-axis \) is the True Positive Rate (TPR) represented by the formula \( TPR = \frac{TP}{TP + FN} \), and since the sum \( TP + FN \) is a very low number, the curve will go very high very fast \cite{precision_recall}.

The Precision-Recall curve's \( y-axis \) is the precision value giving by the formula \( Precision = \frac{TP}{TP + FP} \) and the \( x-axis \) is the recall value \( Recall = \frac{TP}{TP + FN} \) \cite{DataMining}. The area under the curve is denoted as PR AUC and it is our main evaluation criteria.

\subsection{Methods evaluation}

\begin{figure}[h]
\includegraphics[width=8cm]{Binomial.png}
\caption{Precision-Recall curve comparing four binomial problems}
\centering
\label{binomial}
\end{figure}

Figure \ref{binomial} is the model comparison of our four binomial problems. Here we can see that Mito Carta has the highest PR AUC, while Mito disease related genes the worst. This result was not unexpected since we only have 350 Mito disease related genes while 1,158 Mito Carta genes. Even though, Mito Carta appears to be the best model among in this case.

\begin{figure}[h!]
\includegraphics[width=8cm]{Multinomial.png}
\caption{Precision-Recall curve showing the Mito Carta result of the multinomial model}
\centering
\label{multinomial}
\end{figure}

Figure \ref{multinomial} is showing the Mito Carta result of the multinomial problem and, as we can see, it has a lower PR AUC than in the binomial case.

\begin{figure}[h!]
\includegraphics[width=8cm]{Multilabel.png}
\caption{Precision-Recall curve showing the Mitochondrion GO Slim result of the multilabel model}
\centering
\label{multilabel}
\end{figure}

Figure \ref{multilabel} is comparing the five methods used to evaluate the "Problem transformation" approach, using only the GO Slim Mitochondrion as a reference. The first four models (BR, CC, DBR, NST) returned a similar result using PR AUC as a reference, being the fifth model the worst.

\subsection{Tables and Statistics}

Using Mito Carta as reference, we can further evaluate it using statistics measurements, such as \( Accuracy = \frac{TP + TN}{TP + TN + FP + FN} \), \( Sensitivity = \frac{TP}{TP + FN} \) and \( Specificity = \frac{TN}{TN + FP} \) \cite{DataMining}. \\


\begin{table}[h!]
\begin{center}
\begin{tabular}{ |c|c|c| }
 \hline
 & \multicolumn{2}{c|}{Reference} \\
 Prediction & FALSE & TRUE \\
 FALSE & 18640 & 769 \\
 TRUE & 61 & 358 \\
 \hline
\end{tabular}
\caption{Confusion Matrix for Mito Carta}
\label{CM}
\end{center}
\end{table}

\textit{Accuracy : 0.9581}

\textit{Sensitivity : 0.9967}

\textit{Specificity : 0.3177} \\

Table \ref{CM} shows that 358 genes were correctly classified as Mito Carta, while 61 genes were wrongly classified as TRUE. Since we have a high number of TN values, the accuracy is very high for this model. Sensitivity is as good as accuracy also, just specificity dropped 60\% since it takes into consideration the number of TN of the model.

\begin{table}[h!]
\begin{center}
\begin{tabular}{ cc }
 \hline
 \multicolumn{2}{c}{Prediction} \\
 FALSE & TRUE \\
 54534 & 485 \\
 \hline
\end{tabular}
\caption{Number of Mito Carta prediction}
\label{pred}
\end{center}
\end{table}

Table \ref{pred} shows the number of genes predicted as TRUE or FALSE using the default threshold value for prediction of 0.5, meaning if the gene has more than 50\% of chance to be Mito Carta, it will be classified as TRUE. The value 485 represents the sum \( TP + FP \) plus all the non-protein-coding genes classified as Mito Carta, giving a total of 66 possible genes to be further evaluated.

\begin{figure}[h!]
\includegraphics[width=10cm]{cutoff.png}
\caption{Prediction recall values and cutoffs of the Binomial model for Mito Carta}
\centering
\label{cutoff}
\end{figure}

Due to the class imbalance problem in our data, the model will maximize its accuracy by prediction the class which has the majority number of genes \cite{appliedmodeling} which is FALSE in our case. One possible solution is to see the cutoffs values that the Precision-Recall curve used to be constructed and decide the threshold using, for example, the precision value as reference. In our case, as we wanted to have a precision of 50\%, we decided to use 0.211 as threshold (figure \ref{cutoff}).

\begin{figure}[h!]
\includegraphics[width=10cm]{LINC00.png}
\caption{Prediction table of the Binomial model for Mito Carta}
\centering
\label{LINC00}
\end{figure}

One of the non-coding genes found by the algorithm was LINC00116 (figure \ref{LINC00}). This gene is a True Positive example since it was recently discovered to encode a highly conserved muscle-enriched transmembrane microprotein related to mitochondria \cite{LINC00116}.

We also found another probable protein coding gene called LINC00493 (figure \ref{LINC00}) which will be validated in a laboratory using a mouse model to determine if it also encodes a mitochondrial related protein.

\subsection{Performance improvement and additional data}

\begin{figure}[h!]
\includegraphics[width=8cm]{PR_additional_data.png}
\caption{Precision-Recall curve showing the Mito Carta result including different data sources}
\centering
\label{PR_OCR}
\end{figure}

In order to improve prediction performance we tuned the lambda and the alpha parameters of the GLMNET function, resulting in an improvement of 0.13 in the PR AUC when using only GTEx matrix as input and having Mito Carta as output (figure \ref{PR_OCR}). Furthermore, this performance tune reduced the training time of model in 30\% since the algorithm determines the optimal solution faster.

As additional data, we used the OCR values, the CRISPR score and a subset of the compartments score (using the text, psort and yloc scores) that, together with the GTEx data, was used as an input in the \( X \) matrix. Figure \ref{PR_OCR} shows that adding other information sources improve the model, therefore the resulting genes can also be further evaluated.

One difference between using only the GTEx data and combining it with OCR, is that the number of non-protein-coding in the OCR is significantly smaller than previously, so we cannot use this model to predict non protein coding genes as in the previous models, changing the focus to only protein coding genes and its relation to mito diseases.

\section{Conclusion}

This guided research had some interesting results, having one true positive example of a gene which was recently discovered to encode a protein, and also one probable gene which will be further validated in a laboratory.

One issue faced during the research, was the imbalance problem since the real TRUE values of the dataset were much smaller than the real FALSE values. This problem was solved in the evaluation part of the research by using a Precision-Recall curve and in the results parts by not using the default 0.5 threshold value, but changing it accordingly using the cutoffs of the Precision-Recall curve.

As shown in the "Additional data" section, much more can be done using the same algorithm and only improving the input data.

\newpage
\printbibliography

\end{document}
