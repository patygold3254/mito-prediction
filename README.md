## Deciphering the role of ncRNAs in mitochondrial diseases

* Find relations between ncRNAs and our list of mito associated genes
    * Use updated disease genes [wBuild](https://i12g-gagneurweb.in.tum.de/project/genetic_diagnosis/#Scripts_diagnosis_tools_disease_associated_genes.html)

* Find relations between ncRNAs and mitoCarta genes

* Develop a supervised learning method that given the mitoCarta genes as True Positives (Y) and GTEx transcriptomes (X), finds the role of 
ncRNAs and other non-function associated genes.

* Find relations between ncRNAs and Seahorse Bioenergetics


